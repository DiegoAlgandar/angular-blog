import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from 'src/app/models/Usuario'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }


  async AñadirUsuario(usuario: Usuario) {
    var res = await this.http.post(`${environment.API.NewUser}`, usuario).toPromise()
    return res;
  }

}

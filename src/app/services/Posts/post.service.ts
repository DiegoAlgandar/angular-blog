import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Post } from 'src/app/models/Post';
import { Comments_model } from 'src/app/models/Comment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) {

  }
  async ObtenerPosts() {
    var res = await this.http.get(`${environment.API.GetPosts}`).toPromise()
    console.log(res);
    return res;

  }
  async publicarPost(newPost: Post) {
    // newPost.usuario_ID=2;//usuario logeado
    //newPost.categoria_ID=1;//categoria seleccionada
    var res = await this.http.post(`${environment.API.NewPost}`, newPost).toPromise()
    console.log(res);
  }
  async comentar(ID: any) {
    var res = await this.http.put(`${environment.API.NewPost}/${ID}`, ID).toPromise()
    return res;
  }
  async cambiarEstado(ID: any) {
    var res = await this.http.put(`${environment.API.NewPost}/cambiarEstado/${ID}`, ID).toPromise()
    return res;
  }
  async enviarComentario(comment: Comments_model) {
    var res = await this.http.post(`${environment.API.AddComment}`, comment).toPromise()
    return res;
  }
}

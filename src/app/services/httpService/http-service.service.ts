import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(private httpService:HttpClient) { }

  async methodGet(controller,user,password){
    return this.httpService.get<[]>(`${environment.API.url}/${controller}/${user}/${password}`).toPromise();
  }

  async Getgeneric(controller,method){
    return this.httpService.get<[]>(`${environment.API.url}/${controller}/${method}`).toPromise();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Category } from 'src/app/models/Category'


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  async ObtenerCategorias() {

    var res = await this.http.get(`${environment.API.GetCategories}`).toPromise()
    return res
    console.log(res);
  }

  async AñadirCategoria(categoria: Category) {
    var res = await this.http.post(`${environment.API.NewCategory}`, categoria).toPromise()
    console.log(res);
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { PostComponent } from './components/posts/post.component';
import {  SigninComponent} from './components/signin/signin.component';
import { CreatenewentryComponent } from './components/createnewentry/createnewentry.component';
import {  CategoriasComponent} from './components/categorias/categorias.component';



const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'posts', component: PostComponent },
  { path: 'signin', component:  SigninComponent},
  { path: 'newentry', component:  CreatenewentryComponent},
  { path: 'categorias', component:  CategoriasComponent},
  { path: 'create-account', component:  SigninComponent},




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import {HttpServiceService} from '../../services/httpService/http-service.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  formLogin = new FormGroup({
    email : new FormControl('',[Validators.required, Validators.pattern(this.emailPattern)]),
    password : new FormControl('',Validators.required)
  });

  constructor(private http:HttpServiceService) { }

  ngOnInit(): void {
  }

  async IniciarSecion(){

    if(this.formLogin.valid){

      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Datos Correctos, Bienvendio.',
        showConfirmButton: false,
        timer: 1500   
      });
      // controller,method,user,password
     let x=await this.http.methodGet('Users',this.formLogin.value.email,this.formLogin.value.password);
      // waits(1500);
      if(x!=null || x!=[])
      {
        localStorage.setItem("Login",JSON.stringify(x))
      }
       location.replace('/posts');   


    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Error...',
        text: 'Hay espacios en blanco',
      });
    }

  }

}

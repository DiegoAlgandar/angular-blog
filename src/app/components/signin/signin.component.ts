import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/Usuario';
import { UsersService } from 'src/app/services/Users/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user: Usuario;

  private emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  formRegistro = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellidos: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    passwordRepeat: new FormControl('', [Validators.required, Validators.minLength(8)])
  });

  constructor(private userService: UsersService) {
    this.user = new Usuario;
  }

  ngOnInit(): void {

  }

  registrarce() {

    if (this.formRegistro.valid) {

      if (this.formRegistro.get('password').value == this.formRegistro.get('passwordRepeat').value) {
        this.user.nombre = this.formRegistro.get('nombre').value;
        this.user.apellidos = this.formRegistro.get('apellidos').value;
        this.user.email = this.formRegistro.get('email').value;
        this.user.password = this.formRegistro.get('password').value;
        this.user.id = 0;
        this.userService.AñadirUsuario(this.user);
        this.formRegistro.reset();

        location.replace('/login');


      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Error...',
          text: 'Las constraseñas no coinciden',
        })
      }

    }
    else {

      Swal.fire({
        icon: 'error',
        title: 'Error...',
        text: 'Hay espacios en blanco',
      });
    }

  }
  get userControl() { return this.formRegistro.controls }

}

import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Comments_model } from 'src/app/models/Comment';
import { PostService } from 'src/app/services/Posts/post.service';
import Swal from 'sweetalert2';
import { HttpServiceService } from '../../services/httpService/http-service.service'


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor(private PostService: PostService, private http: HttpServiceService) { }
  show: boolean
  PostList: any = []
  busqueda: string
  newComment: Comments_model


  async ngOnInit() {
    this.busqueda = ''
    this.newComment = new Comments_model();
    this.show = false
    this.PostList = await this.http.Getgeneric('post', 'obtenerEntradas');
    console.log(this.PostList)
    //
    await this.PostList.map(x => this.CambiarEstados(x))
  }


  async Comentar(Show: any, ID: any) {
    await this.PostService.comentar(ID)
    this.ngOnInit()
  }

  async SendComment(comment: Comments_model, ID: any) {
    const User = JSON.parse(localStorage.getItem('Login'))
    comment.id_publicacion = await ID
    comment.id_usuario = await User.id;
    var res = await this.PostService.enviarComentario(comment)
    this.ngOnInit()
  }

  async CambiarEstados(post) {
    if (post.showComments) {
      return await this.PostService.cambiarEstado(post.id)

    } else return null

  }

  buscar(titulo: string) {
    this.show = true
    this.PostList = this.PostList.filter(x =>
      x.titulo.toLowerCase().includes(titulo.toLowerCase().trim())
    )
    console.log(this.PostList);

  }

  cancelar() {
    this.ngOnInit()
  }

  async comentarPrueba(ID: any) {
    const { value: comentario } = await Swal.fire({
      title: 'Agregar comentario',
      input: 'textarea',
      showCancelButton: true
    })

    if (comentario) {
      this.newComment.comentario = comentario
      if(this.newComment.comentario.trim() != ''){
        this.SendComment(this.newComment, ID)        
      }
    }   


  }



}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  Links = [
    { name: "Publicaciones", url: "/posts" },
    { name: "Crear nuevo post", url: "/newentry" },
    { name: "Agregar nueva categoria", url: "/categorias" },

  ]

  constructor() { }

  ngOnInit(): void {
  }
  async cerrarSesion(){
    await localStorage.removeItem('Login')
    location.replace('/login')

  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Post } from 'src/app/models/Post';
import { CategoriesService } from 'src/app/services/categories/categories.service';
import { PostService } from 'src/app/services/Posts/post.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-createnewentry',
  templateUrl: './createnewentry.component.html',
  styleUrls: ['./createnewentry.component.css']
})
export class CreatenewentryComponent implements OnInit {

  categorias:any=[];
  formPost = new FormGroup({
    titulo: new FormControl('', Validators.required),
    categoria: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required)
  });
  newPost: Post
  constructor(private PostService: PostService, private CategoryService: CategoriesService) { }

  ngOnInit(): void {
    this.newPost = new Post();
    this.obtenerCategorias()
  }

  publicarPost() {
    
    console.log(this.formPost.valid);
    if (this.formPost.valid) {
    let user=JSON.parse(localStorage.getItem('Login'))
    this.newPost.usuario_ID=user.id
    console.log(this.newPost)


      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'El poste se ha realizado correctamente!',
        showConfirmButton: false,
        timer: 1500
      })
      this.PostService.publicarPost(this.newPost);

      location.replace('/posts');

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Error...',
        text: 'Hay espacios en blanco',
      })
    }
  }
async   obtenerCategorias(){
    this.categorias=await this.CategoryService.ObtenerCategorias()
    console.log(this.categorias)
  }

}

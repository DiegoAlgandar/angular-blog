import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatenewentryComponent } from './createnewentry.component';

describe('CreatenewentryComponent', () => {
  let component: CreatenewentryComponent;
  let fixture: ComponentFixture<CreatenewentryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatenewentryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatenewentryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

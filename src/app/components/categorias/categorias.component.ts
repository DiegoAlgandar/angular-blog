import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/Category';
import { CategoriesService } from 'src/app/services/categories/categories.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {
  newCategory: Category

  constructor(private categoryService: CategoriesService) {
    
  }

  ngOnInit(): void {
    this.newCategory = new Category()
  }
  NuevaCategoria(categoria: Category) {
    if(categoria.nombre){
      this.categoryService.AñadirCategoria(categoria)        
    }
    Swal.fire({
      icon: 'error',
      title: 'Error...',
      text: 'No se ha escrito ninguna categoria.',
    });
    
  }

}
